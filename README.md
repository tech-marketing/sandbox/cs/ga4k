# CS-Skills-Exchange-Apr-20-2022

This lab will guide you through the steps to create, configure and deploy your own GitLab agent for K8s to a running GKE cluster. During the lab, you will be modifying and adding files to the repository. To do so, you can either clone the repo to your laptop, or directly use the Web IDE on your browser.

## Pre-requisites

In order to take part in the hands-on session, you'll need a few applications installed on your laptop:

- [gcloud CLI](https://cloud.google.com/sdk/docs/install). **NOTE: you may need to run `gcloud init` and/or `gcloud auth login` to get gcloud running properly on your laptop.**
/* - [docker or alternative](https://rancherdesktop.io/) - for more information on alternative check our [handbook page](https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop) */
- [helm](https://helm.sh/docs/intro/install/)
- [kubectl](https://kubernetes.io/docs/tasks/tools/)
- You should have created a merge request (MR) for the creation of your personalized namespace per the instructions in the following [issue](https://gitlab.com/tech-marketing/sandbox/cs/ga4k/-/issues/1). This MR must contain ONLY the updates in the instructions and nothing else. This MR will be merged by the instructor during the lab. **PLEASE DO NOT MERGE THIS MR TO THE MAIN BRANCH.**

You will have been added as a Kubernetes Engine Admin to GCP ahead of this lab. We will share the kubeconfig command to run in order to access the running GKE cluster during the lab. In addition, you will have been added to this project as a Maintainer.

## Hands-on lab steps - Setting up the agent

### Checking connectivity to GKE from your laptop

1. Open a Terminal window on your laptop and enter the kubeconfig command that will be shared with you during the lab to connect to the running GKE. The command will look like this (during the lab, it will be different):

```
gcloud container clusters get-credentials <CLUSTER NAME> --zone <GCP ZONE> --project <GCP PROJECT NAME>
```

The command above will add an entry to your `${HOME}.kube/config` file on your laptop so that you can execute kubectl commands against the running GKE, for example.

2. Check connection to the GKE cluster by entering the following command from your Terminal window:

```
kubectl config get-contexts
kubectl get pods --all-namespaces
```

> NOTE: At this point, you could have a look at the cluster using the lens or k9s tools, if you have them installed and configured on your laptop.

### Create a merge request for configuring your own agent

3. Head over to project **https://gitlab.com/tech-marketing/sandbox/cs/ga4k** and create a **brand new MR** that creates an agent config file `.gitlab/agents/<your handle>/config.yaml`. **IMPORTANT NOTE:** **Use the Web IDE** so that you don't have to manually create a branch for the MR. In the Web IDE, ensure that you select the radio button **Create a new branch**  AND that the box **Start a new merge request** is checked BEFORE you click on the **Commit** button.

4. Paste the following content into the newly created file:

> NOTE: replace <your handle> with your GitLab handle. Leave the rest of the text the same. The manifest path should point directly to the participant's namespace file.

<pre>
gitops:
  manifest_projects:
  - id: tech-marketing/sandbox/cs/ga4k
    default_namespace: gitlab-agent
    paths:
      # Change the following line
    - glob: 'manifests/<mark><b>&lt;your handle&gt;</b></mark>/*.{yaml,yml,json}'
</pre>

5. Commit the updates to the specific MR feature branch for these changes. Enter the words "Agent configuration" in the MR Commit message. **NOTE: Make sure that this is a BRAND NEW MR by ensuring that the radio button "Create a new branch" is selected and that the box "Start a new merge request" is checked. DO NOT add these updates to the MR from the pre-requisite section. This MR must contain ONLY the updates from step 4. PLEASE DO NOT MERGE THIS MR TO THE MAIN BRANCH.**

6. At this point, ensure that the instructor merges your MR above to the main branch.

### Registering your agent with GitLab

7. Now, you need to register the agent. Back in project **https://gitlab.com/tech-marketing/sandbox/cs/ga4k**, select **Infrastructure > Kubernetes clusters** from the left vertical menu.

8. From the Kubernetes window, click on the **Connect a cluster (agent)** button on the top right side of the screen. **NOTE: If the button is greyed out, ask the instructor to make you a maintainer in the project.**

9. From the pop-down list, select the agent with your GitLab handle as its name. Then click on the **Register** button.

10. The next screen will show a `helm ...` commands. **IMPORTANT NOTE: DO NOT PASTE this command in your Terminal window at this point. Instead, copy the command and save it to a local file on your laptop.** This command will install your GitLab Agent on the running GKE cluster.

11. On your local file where you pasted the command, prepend `<your handle>-` to the value of the `--install` parameter in the previous `helm upgrade ...` command as follows:

<pre>
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install <mark><b>&lt;your handle&gt;-</b></mark>gitlab-agent gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set config.token=xxxxxxxxxxxx \
    --set config.kasAddress=wss://kas.gitlab.com
</pre>

### Deploying your agent to GKE

12. Paste the updated `helm ...` commands from the previous step in a Terminal window on your laptop and press return to execute it.

13. Get the name of the pod for your agent with the following command:

```
kubectl get pods --namespace gitlab-agent
```

> NOTE: identify your agent pod!

14. Open a brand new Terminal window and start trailing your agent's log output by entering the following command, where you need to specify the name of the pod for your agent (that you identified in the previous step):

<pre>
kubectl logs -f <mark><b>&lt;name of the pod for your agent&gt;</b></mark> --namespace gitlab-agent
</pre>

## Hands-on lab steps - Have the agent create your namespace in the GKE cluster

13. At this point, ensure that the instructor merges the MR with the feature branch for the creation of your personalized namespace that was part of the pre-requisite of this lab.

14. Keep an eye on the Terminal window where you are trailing the log output of your agent. You should see the agent detect the new `<your handle>-ns.yaml` file in `manifests/<your handle>` and apply the contents of the file to the GKE cluster, effectively creating your new personalized namespace.

15. To verify that your personalized namespace was created by the agent, enter the following command from a Terminal window:

```
kubectl get namespace
```

You should see your personalized namespace in the listof namespaces.

## Hand-on lab steps - Going through a simple GitOps flow

Now that you have created your personalized namespace, let's go through a quick GitOps flow to deploy an NGINX server to the cluster under your namespace.

16. Back in project **https://gitlab.com/tech-marketing/sandbox/cs/ga4k**, select **Issues** from the left vertical menu.

17. Create a new issue and give it the title "Provision nginx servers". Assign the issue to yourself.

18. From inside the newly created issue, create a merge request.

19. From the newly created merge request, launch the Web IDE.

20. From inside the Web IDE, navigate to your personalized manifests directory `manifests/<your handle>` and create a new file under it. Name this file `nginx.yaml` and paste the following content into it:

**NOTE:** you will need to replace the string `<your namespace that includes your handle>` in the content below with the namespace you defined in the [pre-requisite](https://gitlab.com/tech-marketing/sandbox/cs/ga4k/-/blob/main/README.md#pre-requisites) MR.

<pre>
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  namespace: <mark><b>&lt;your namespace that includes your handle&gt;</b></mark>  # Can be any namespace managed by you that the agent has access to.
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 1
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
          protocol: TCP
</pre>

21. At this point, ensure that the instructor merges the MR with the feature branch for the creation of your nginx server.

22. Keep an eye on the Terminal window where you are trailing the log output of your agent. You should see the agent detect the new `nginx.yaml` file in `manifests/<your handle>` and apply the contents of the file to the GKE cluster, effectively creating an instance of nginx under your namespace. If you'd like to check the cluster for the pods under your namespace, you can enter the following command from a Terminal window:

```
kubectl get pods -n <your namespace that includes your handle>
```

# Setting up the GitLab workflow for Kubernetes, a.k.a. The CI/CD Tunnel (DO NOT DO THE STEPS IN THIS SECTION - Instructor will demo)

## Create adjacent project sample-application

23. Head over to the **https://gitlab.com/tech-marketing/sandbox/cs** subgroup and create a new project in this subgroup and name it `sample-application`.

## Update your agent configuration

24. From inside the project **https://gitlab.com/tech-marketing/sandbox/cs/ga4k**, go to the folder `.gitlab/agents/<your handle>`, and click on the file `config.yaml`.

25. Edit the configuration of the agent by clicking the *Edit* button to the right side of the file name.

26. Once in the **Edit file** window, append the following content at the bottom of the file (make sure to keep the lines' indentation as you append the text below):

<pre>
observability:
  logging:
    level: debug
ci_access:
  projects:
  - id: "tech-marketing/sandbox/cs/sample-application"
</pre>

27. Commit your changes to the main branch.

## Create a pipeline in the sample-application project

28. Head over to **https://gitlab.com/tech-marketing/sandbox/cs** subgroup and then click on the project `sample-application`.

29. Inside the `sample-application` project, select **New file** from the *+* popdown menu next to the name of the project.

30. Name the file `.gitlab-ci.yml` and paste the following contents into it:

<pre>
variables:
# Set the variable $KUBE_CONTEXT through the GitLab UI, or set it here by
# uncommenting the following two lines and replacing the Agent's path with your own:
# variables:
  KUBE_CONTEXT: "tech-marketing/sandbox/cs/ga4k:<mark><b>&lt;name of your agent, which should be your handle&gt;</b></mark>"

.kube-context:
  before_script:
    - if [ -n "$KUBE_CONTEXT" ]; then kubectl config use-context "$KUBE_CONTEXT"; fi

test:
   extends: [.kube-context]
   stage: test
   image:
     name: bitnami/kubectl:1.21
     entrypoint: [""]
   script:
   - kubectl config get-contexts
   - kubectl get pods --all-namespaces
</pre>

31. Commit your changes to the main branch.

## Start trailing the log of the GitLab Agent for Kubernetes

Before we start running pipelines, let's start trailing the log of the Agent so that you can observe its log messages as changes are introduced to the infrastructure.

32. Open a brand new Terminal window and start trailing your agent's log output by entering the following command, where you need to specify the name of the pod for your agent (that you identified in an earlier step above):

<pre>
kubectl logs -f <mark><b>&lt;name of the pod for your agent&gt;</b></mark> --namespace gitlab-agent
</pre>

## Impersonating the agent

Now let's run the pipeline you just created in the step 30 above. The default is for this pipeline to run impersonating the agent itself.

33. From the left vertical navigation menu of your `sample-application` project, select **CI/CD > Pipelines**.

34. On the Pipelines screen, click on the **Run pipeline** button on the top right.

35. From the **Run pipeline** screen, click on the **Run pipeline** button ensuring that the main branch is selected. There is no need to specify any variable values from this screen.

36. Once the pipeline is running, you will see a single job **test**. Click on the **test** job node to go to its log output window.

37. From the job log output window, you will see that the pipeline executes the two *kubectl* commands successfully.

## Impersonating the CI job

Now, let's update the agent configuration to impersonate the CI job.

38. Head over to the project **https://gitlab.com/tech-marketing/sandbox/cs/ga4k**.

39. From inside the project, go to the folder `.gitlab/agents/<your handle>`, and click on the file `config.yaml`.

40. Edit the configuration of the agent by clicking the *Edit* button to the right side of the file name.

41. Once in the **Edit file** window, append the following content at the bottom of the file (make sure to keep the lines' indentation as you append the text below):

<pre>
    access_as:
      ci_job: {}
</pre>

42. Commit your changes to the main branch.

43. As you commit the changes to your agent configuration, you can view the command window from which you're trailing the agent's log output to verify that the agent reconfiguration has taken place.

44. From the left vertical navigation menu of your `sample-application` project, select **CI/CD > Pipelines**.

45. On the Pipelines screen, click on the **Run pipeline** button on the top right.

46. From the **Run pipeline** screen, click on the **Run pipeline** button ensuring that the main branch is selected. There is no need to specify any variable values from this screen.

47. Once the pipeline is running, you will see a single job **test**. Click on the **test** job node to go to its log output window.

48. From the job log output window, you will see that the pipeline executes but only one of the two *kubectl* commands is successful. The `kubectl get pods --all-namespaces` fails because the pipeline is executing with a unique impersonation that does not have the permission to list pods in your cluster. To fix this failure, you would need to create a cluster role and binding for the specific impersonation. You will learn how to do this in the next section so let's skip this failure for now. For more information on the CI job impersonation, please check out the [documentation](https://docs.gitlab.com/ee/user/clusters/agent/repository.html#impersonate-the-ci-job-that-accesses-the-cluster).

## Impersonating a specific service account in your K8s cluster

Now, let's update the agent configuration to impersonate a specific service account in your Kubernetes cluster. But before we impersonate a service account, let's create it in your Kubernetes cluster. 

49. Open a Terminal window and enter the following command to create a service account *jane*:

> kubectl create serviceaccount jane

50. From the same Terminal window, enter the following to create a cluster role for listing the pods in your Kubernetes cluster:

> kubectl create clusterrole getpodsrole --verb=get --verb=list --verb=watch --resource=pods

51. From the same Terminal window, enter the following to give *jane* the ability to list the pods in your Kubernetes cluster:

> kubectl -n default create clusterrolebinding allowgetpodsrole --clusterrole=getpodsrole --serviceaccount=default:jane

52. You also need to give the GitLab Agent for Kubernetes, installed in your cluster, the permission to impersonate another service account. From the same command window you have been using, enter the following command to create an *impersonator* cluster role:

> kubectl create clusterrole impersonator --verb=impersonate --resource=users --resource=groups --resource=serviceaccounts

53. During the deployment of the agent in step 11 above, a service account `<your handle>-gitlab-agent` and a namespace `gitlab-agent` were created in your Kubernetes cluster when you deployed the GitLab Agent for Kubernetes to your cluster. Let's use these service account and namespace to give the impersonation permission to your agent. From the same command window, enter the following command to give the agent the permission to impersonate another service account:

> kubectl -n default create clusterrolebinding allowimpersonator --clusterrole=impersonator --serviceaccount=gitlab-agent:\<your handle\>-gitlab-agent

54. Head over to the project **https://gitlab.com/tech-marketing/sandbox/cs/ga4k**, go to the folder `.gitlab/agents/<your handle>`, and click on the file *config.yaml*.

56. Edit the configuration of the agent by clicking the *Edit* button to the right side of the file name.

57. Once in the **Edit file** window, replace the entire `access_as:` portion at the bottom of the file with the following content (make sure to keep the lines' indentation as you append the text below)::

<pre>
    access_as:
      impersonate:
        username: "system:serviceaccount:default:jane"
</pre>

58. Commit your changes to the main branch.

59. As you commit the changes to your agent configuration, you can view the command window from which you're trailing the agent's log output to verify that the agent reconfiguration has taken place.

60. From the left vertical navigation menu of your *sample-application* project, select **CI/CD > Pipelines**.

61. On the Pipelines screen, click on the **Run pipeline** button on the top right.

62. From the **Run pipeline** screen, click on the **Run pipeline** button ensuring that the main branch is selected. There is no need to specify any variable values from this screen.

63. Once the pipeline is running, you will see a single job **test**. Click on the **test** job node to go to its log output window.

64. From the job log output window, you will see that the pipeline executes the two *kubectl* commands successfully.

Congratulations! You have completed this lab.

## References

The following material was used in the preparation of this lab. These references also include further information related to positioning and benefits of the GitLab Agent for Kubernetes:

1. [Setting up the GitLab Agent for Kubernetes for the pull-based approach to GitOps](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/11.%20GitOps%20with%20GitLab.md#setting-up-the-gitlab-agent-for-kubernetes-for-the-pull-based-approach-to-gitops)
2. [Using the CI/CD tunnel with the GitLab Agent for Kubernetes for the push-based approach to GitOps](https://gitlab.com/tech-marketing/workshops/cicd/cd-labs-instructions/-/blob/master/11.%20GitOps%20with%20GitLab.md#using-the-cicd-tunnel-with-the-gitlab-agent-for-kubernetes-for-the-push-based-approach-to-gitops)
